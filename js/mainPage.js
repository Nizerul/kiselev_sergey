//Инициализация переменных

var booksNumber = 0,
    allBooksArray;

//Инициализация поля для перетаскивания книг

$(function() {
	
    $('#books').sortable();  

});

//Загрузка книг на главной странице

$.get( link + "book/", function( data ) {
    allBooksArray = data;  
    
    $.each(allBooksArray, function(key, book){
        if(booksNumber < 4){
            bookInsert($('#books'), book, booksNumber);
            booksNumber++;
        }else if( booksNumber == 4){
            allBooksArray.splice(0,4);
            booksNumber++;
        }
    })

});

//Нажатие на кнопку "Еще несколько книг"

$('#more-book').mousedown(function(){
    $(this).css('transform', 'scale(0.9)');
})

$('#more-book').mouseup(function(){
    $(this).css('transform', 'scale(1)');
})

$('#more-book').click(function(){
    booksNumber = 0;
    $.each(allBooksArray, function(key, book){
        if(booksNumber < 4){
            bookInsert($('#books'), book);
            booksNumber++;
            if(booksNumber == allBooksArray.length){
                $('#more-book').css('display', 'none');
            }
        }else if( booksNumber == 4){
            allBooksArray.splice(0,4);
            booksNumber++;
        }
    })
})