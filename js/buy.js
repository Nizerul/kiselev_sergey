//Инициализируем переменные

var bookPrice,
    deliveryPrice = 0,
    deliveryCurrency,
    bookCurrency,
    bookId = GetURLParameter('bookid');

//Функция вывода способов доставки

function paymentInsert(payment){
    $('.payment-select').append($('<input type="radio" name="payment" class="payment" required>')
                               .attr('id', payment.id)
                               .attr('value', payment.id))
                        .append($('<label for="payment"></label>').html(payment.title))
                        .append($('<br>'));
}

//Вывод информации о выбранной книге
    
$.get( link + "book/" + bookId, function( book ) {
    $('.book-name').html('Оформить заказ на книгу <a href="detail.html?bookid=' + book.id + '&currency=' + globalCurrencyId + '">' + book.title +'</a>');
    $('.book-cover').attr('src', book.cover.large);
    bookPrice = book.price;
    bookCurrency = book.currency;
    $('.total').html('Итого к оплате: ' + bookPrice + ' USD');
})

//Загрузка способов доставки
         
$.get( link + "order/delivery", function( delivery ) {
    $.each(delivery, function(key, delivery){
        $('.delivery-select').append($('<input type="radio" name="delivery" class="delivery">').attr('id', delivery.id)
                   .attr('data-price', delivery.price)
                   .attr('data-adress', delivery.needAdress)
                   .attr('data-currency', delivery.currency)
                   .attr('value', delivery.id))
                             .append($('<label for="delivery"></label>').html(delivery.name + " - " + delivery.price + " USD"))
                             .append($('<br>'));
    })
    
    //При загрузке страницы отмечаем первый пункт
        
    $('.delivery-select input:first').attr('checked', true)
                                     .attr('required', true);

    //При загрузке страницы выводятся способы оплаты для первого пункта
    
    $.get( link + "order/delivery/delivery-01/payment", function( payment ) {
        $.each(payment, function(key, payment){
            paymentInsert(payment)
        })
    })
        
    // Выбор способа доставки.

    $(".delivery").click(function(){

        // При выборе обновляется список способов оплаты

        $('.payment-select').html('<p>Способ оплаты:</p>');
        $.get( link + "order/delivery/" + $(this).attr('id') + "/payment", function( payment ) {
            $.each(payment, function(key, payment){
                paymentInsert(payment)
            })
        })

        // Проверка необходимости адреса

        if($(this).attr('data-adress') == "true"){
            $('.adress').css('display', 'block');
            $('.adress input').attr('required', 'true');
        }else{
            $('.adress').css('display', 'none')
            $('.adress input').attr('required', false);
        }


        // Рассчет конечной стоимости

        deliveryPrice = $(this).attr('data-price');

        total = Number(bookPrice) + Number(deliveryPrice);

        $('.total').html("Итого к оплате: " + total + " " + $('option:selected').attr('data-charcode'));
    })         
})
    
//Нажатие кнопки "Оформить заказ"
    
$('#buyForm').submit(function(e){
    e.preventDefault();
    var data = {
        'manager' : 'KSS89066905225@yandex.ru',
        'book' : bookId,
        'name' : $(this).find('input[name="name"]').val(),
        'phone' : $(this).find('input[name="phone"]').val(),
        'email' : $(this).find('input[name="email"]').val(),
        'comment' : $(this).find('textarea[name="comment"]').val(),
        'delivery' : {
            'id' : $(this).find('input[name="delivery"]:checked').val(),
            'address' : $(this).find('input[name="adress"]').val()
        },
        'payment' : {
            'id' : $(this).find('input[name="payment"]:checked').val(),
            'currency' : $('option:selected').val(),
        }
    }
        
    $.ajax({
        type: "POST",
        url: link + "order",
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        beforeSend: function(){
            $('#buyForm').animate({opacity: 0}, 1000);
            $('.book-cover').animate({opacity: 0}, 1000, function(){
                $('.loader').animate({opacity: 1}, 1000);
            });
        },
        success: function(result){
            $('.success').css('display', 'block');
            $('.loader').animate({opacity: 0}, 1000, function(){
                $('.success').animate({opacity: 1}, 1000);
            });
        },
        error: function(){
            $('.loader').animate({opacity: 0}, 1000, function(){
                $('#buyForm').animate({opacity: 1}, 1000);
                $('.book-cover').animate({opacity: 1}, 1000);
            });
            alert("Пожалуйста, проверьте еще раз введеную вами информацию")
        }
    })
})