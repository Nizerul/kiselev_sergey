//Инициализируем переменные

var bookId = GetURLParameter('bookid'),
    bookCurrency,
    bookCost;

//Вывод книги
    
$.get( link + "book/" + bookId, function( book ) {
    
    $('#book').append($('<div class="book"></div>')
              .append($('<div class="book-cover"></div>')
              .append($('<div id="eye" class="eye"></div>')
              .append($('<div class="eyelid top-eyelid"></div>'))
              .append($('<div class="eyelid bottom-eyelid"></div>'))
              .append($('<div id="pupil" class="pupil"></div>')))
              .append($('<img>').attr("src", book.cover.large)))
              .append($('<p class="book-description"></p>').html(book.description)));
    
    //Вставка блока обзоров
    
    $('#book').append($('<div id="reviews-block" class="reviews-block"></div>'));
    $.each(book.reviews, function(key, review){
        $('#reviews-block').append($('<div class="review-block"></div>')
                           .append($('<img>').attr("src", review.author.pic))
                           .append($('<p></p>').html(review.cite)));
    });
    
    //Вставка блока особенностей
    
    $('#book').append($('<div id="features-block" class="features-block"></div>'));
    $.each(book.features, function(key, feature){
        $('#features-block').append($('<div class="feature-block"></div>')
                            .append($('<img>').attr("src", feature.pic))
                            .append($('<p></p>').html(feature.title)));
    });
    
    // Отрисовка глаза и зрачка
    
    setTimeout(function(){
        var eyeSize = $('.book-cover img').width() / 3,
            eyeLeft = $('.book-cover').width() / 2 - eyeSize / 2,
            eyeTop = $('.book-cover').height() / 2,
            pupilSize = eyeSize * 0.18,
            pupilPosition = eyeSize / 2 - pupilSize / 1.5;

        $('.eye').css('width', eyeSize)
                 .css('height', eyeSize)
                 .css('left', eyeLeft)
                 .css('top', eyeTop);
        $('.pupil').css('width', pupilSize)
                   .css('height', pupilSize)
                   .css('top', pupilPosition)
                   .css('left', pupilPosition);

        var DrawEye = function(eyeball, pupil, eyeposx, eyeposy){

          // Инициализация переменных

          var r = $(pupil).width()/2;
          var center = {
            x: $(eyeball).width()/2 - r, 
            y: $(eyeball).height()/2 - r
          };
          var distanceThreshold = $(eyeball).width()/2 - r;
          var mouseX = 0, mouseY = 0;

          // Реакция на движение мыши

          $(window).mousemove(function(e){

            var d = {
              x: e.pageX - r - eyeposx - center.x,
              y: e.pageY - r - eyeposy - center.y
            };
            var distance = Math.sqrt(d.x*d.x + d.y*d.y);
            if (distance < distanceThreshold) {
              mouseX = e.pageX - eyeposx - r;
              mouseY = e.pageY - eyeposy - r;
            } else {
              mouseX = d.x / distance * distanceThreshold + center.x;
              mouseY = d.y / distance * distanceThreshold + center.y;
            }
          });

          // Обновление положения зрачка

          var pupil = $(pupil);
          var xp = 0, yp = 0;
          var loop = setInterval(function(){
            xp += (mouseX - xp) / 1;
            yp += (mouseY - yp) / 1;
            pupil.css({left:xp, top:yp});    
          }, 1);
        };

        DrawEye("#eye", "#pupil", $('.eye').offset().left, $('.eye').offset().top);
        
        $('.detail-book, .buy-button').animate({opacity: 1}, 1000);
        
    }, 800);
        
    //Кнопка "Купить"
        
    if( window.innerWidth >= 1366 ){
        $('.buy-button').html('<a href="buy.html?bookid=' + book.id + '&currency=' + globalCurrencyId + '">Купить за жалкие ' + book.price + ' Z</a>');  
    }else{
        $('.buy-button').html('<a href="buy.html?bookid=' + book.id + '&currency=' + globalCurrencyId + '">Купить<br> за жалкие ' + book.price + ' Z</a>');  
    }
    
    bookCost = book.price;
    bookCurrency = book.currency;
    
    return bookCost, bookCurrency;
        
});