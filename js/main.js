//Инициализация общей переменной

var link = "https://netology-fbb-store-api.herokuapp.com/",
    pathnameArray = window.location.pathname.split('/'),
    pathName,
    globalCurrencyId,
    allBooks,
    newCost;

//Собираем все книги

$.get( link + "book/", function( data ) {
    allBooks = data;
    return allBooks;
});

//Функция вставки книги

function bookInsert(insertBlock, book){
    insertBlock.append($('<div class="book clearfix"></div>')
               .append($('<div class="book-cover"></div>').html('<a href="detail.html?bookid=' + book.id + '&currency=' + globalCurrencyId + '"><img src="' + book.cover.small + '"></a>'))
               .append($('<a class="book-info"></p>').html(book.info).attr('href', 'detail.html?bookid=' + book.id + '&currency=' + globalCurrencyId)));
}

//Функция взятия параметров из адресной строки

function GetURLParameter(sParam){
    
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
        
}

// Берем валюту из адресной строки

globalCurrencyId = GetURLParameter('currency');

if( globalCurrencyId == undefined){
    globalCurrencyId = 'R01235';
}

//Определяем страницу
    
$.each(pathnameArray, function(key, pathname){
    if (pathname == '' || pathname == 'index.html' || pathname == 'buy.html' || pathname == 'detail.html' || pathname == 'about.html' ){
        pathName = pathname;
        return pathName;
    }
})

//Функция изменения ссылки

function hrefChange(changeHref, currencyId){
    
    var oldHref = $(changeHref).attr('href'),
        newHref = oldHref.replace(/currency=R\d+\w+/, "currency=" + currencyId);
        $(changeHref).attr('href', newHref);
    
}

//Функция пересчета стоимости книги и способов доставки по новой валюте

function priceCalc(oldCost, valueVariable, nominalVariable, currToValue, currToNominal){
    
    var currFromValue = $('option[value="' + valueVariable + '"]').attr('data-value'),
        currFromNominal = $('option[value="' + nominalVariable + '"]').attr('data-nominal');
    newPrice = (oldCost * (currFromValue / currFromNominal) / (currToValue / currToNominal)).toFixed(0);
    return newPrice;
    
}

//Функция присвоения ссылок с валютами и расчет стоимости книги по курсу валюты

function href(currencyId){
    
    var currToValue = $('option:selected').attr('data-value'),
        currToNominal = $('option:selected').attr('data-nominal'),
        newCharcode = $('option:selected').attr('data-charcode');
    
    //Если главная страница
    
    if ( pathName == '' || pathName == 'index.html'){
        $.each($('.book-cover a, .book-info'), function(hey, href){
            hrefChange(href, currencyId);
        });
    }
    
    //Если страница с детальным описанием книги
    
    if ( pathName == 'detail.html' ){
        
        var newBookPrice = priceCalc(bookCost, bookCurrency , bookCurrency , currToValue, currToNominal);
        
        var oldText = $('.buy-button a').text(),
            newText = oldText.replace(/жалкие \w+ \w+/, "жалкие " + newBookPrice + ' ' + newCharcode);
        $('.buy-button a').text(newText);
        
        hrefChange($('.buy-button a'), currencyId);
        
    }
    
    //Если страница с оформлением заказа
    
    if ( pathName == 'buy.html' ){
        hrefChange($('.book-name a'), currencyId);
        
        $.each($('.delivery-select input'), function(key, input){
            
            var deliveryOldPrice = $(input).attr('data-price'),
                deliveryOldCurrency = $(input).attr('data-currency');
            
            newDeliveryPrice = priceCalc(deliveryOldPrice, deliveryOldCurrency , deliveryOldCurrency , currToValue, currToNominal)
            
            $(input).attr('data-price', newDeliveryPrice );
            $(input).attr('data-currency', currencyId);
            
            var oldDeliveryText = $(this).next('label').text(),
                newDeliveryText = oldDeliveryText.replace(/- \w+ \w+/, "- " + newDeliveryPrice  + ' ' + newCharcode);
            $(this).next('label').text(newDeliveryText);
            
        });
        
        var newBookPrice = priceCalc(bookPrice, bookCurrency , bookCurrency , currToValue, currToNominal),
            deliveryPrice = $('input[name="delivery"]:checked').attr('data-price'),
            newTotal = Number(newBookPrice) + Number(deliveryPrice),
            oldTotalText = $('.total').text(),
            newTotalText = oldTotalText.replace(/: \w+ \w+/, ": " + newTotal + ' ' + newCharcode);
            $('.total').text(newTotalText);
        
        bookPrice = newBookPrice;
        bookCurrency = $('option:selected').val();
    }
    
    if ( pathName != '/' || pathName != '/index.html'){
        $('header a').attr('href','index.html?currency=' + currencyId);
    }
    if ( pathName != 'about.html'){
        $('footer a').attr('href','about.html?currency=' + currencyId);
    }
}

//Загрузка списка валют

$.get( link + "currency/", function( currency ) {
    
    $.each(currency, function(key, currency){
        $('#currency').append($('<option data-value="' + currency.Value + '" data-nominal="' + currency.Nominal + '" data-charcode="' + currency.CharCode + '" value="' + currency.ID + '">' + currency.Name + '</option>'));
    });
    
    $('option[value="' + globalCurrencyId + '"]').attr("selected", "selected");
    
    href(globalCurrencyId);
});

// Изменяем валюту

$('#currency').change(function(){
    
    var currencyId = $(this).val();
        globalCurrencyId = currencyId;
    
    href(currencyId);
    
})

//Функция поиска книги

$('.search-form').submit(function (e){
    e.preventDefault();
})
      

$('.search').keyup(function(){
    
    var searchValue = $(this).val().toLowerCase();
    
    if( searchValue != '' ){
        $('.search-content').html('');
        $('.page-content').css('display', 'none');
        $('.search-content').css('display', 'block');
        $('.search-content').append($('<div id="search-book" class="books"></div>'));
        
        var i = 0;
        
        $.each(allBooks, function(key, book){
                
            if(book.author.name.toLowerCase().indexOf(searchValue) != -1 || book.title.toLowerCase().indexOf(searchValue) != -1){
                
                bookInsert($('#search-book'), book);
                i++;
            }
        })
            
            if( i == 0 ){
                $('#search-book').append($('<h2></h2>').text('Нет похожих товаров'))
            }

    }else{
        $('.page-content').css('display', 'block');
        $('.search-content').css('display', 'none');
    }
    
})